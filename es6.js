//////////////////
// OLD JAVASCRIPT
//////////////////

// var names = ['Ed', 'John', 'Mike'];
//
// var counter = 10;
//
// var counter = 5;
// console.log(counter);
//
// function sayName() {
//     var name = 'ed';
//     console.log(name)
//  }
//
// sayName();

// var name = 'David';
// console.log('Hello my name is ' + name);

//
// function getBook(title, author) {
//     return {
//         title: title,
//         author: author,
//         nonFunctionVariable: true,
//     }
// }
//
// var book = getBook('Harry Potter', 'JK');
//
// console.log(book);

// var user = {
//     name: 'Ed',
//     age: 25,
// };
//
// var myName = user.name;
//
// console.log(myName);

// function sayName() {
//     console.log('Hello I am Brian');
// }
//
// var sayAge = function () {
//     console.log('My age is 23');
// };
//
// sayAge();
// sayName();

// var user = {
//     name: 'David',
//     age: 23,
//     sayName: function () {
//         console.log('My name is ' + this.name);
//         var that = this;
//         var fullName = function () {
//             console.log('My name is ' + that.name + ' and my age is ' + that.age);
//         };
//         fullName();
//     }
// };
//
// user.sayName();
//
// function multiply(x, y) {
//     var a = x || 1;
//     var b = y || 1;
//     console.log(a * b);
// }
//
// multiply(12);
//
// function Person(name, age, hairColor) {
//     this.name = name;
//     this.age = age;
//     this.hairColor = hairColor;
// }
//
// Person.prototype.sayName = function () {
//     console.log('My name is ' + this.name);
// };
//
// var David = new Person('David', 23, 'nice');
//
// console.log(David);
// David.sayName();
//
// function Ed(occupation, hobbies, name, age, hairColor) {
//     Person.call(this, name, age, hairColor);
//     this.occupation = occupation;
//     this.hobbies = hobbies;
// }
//
// Ed.prototype = Object.create(Person.prototype);
//
// const person = new Ed('dev', 'games', 'David', 23, 'black');
//
// person.sayName()

// console.log('start');
//
// function getData(data, callback) {
//     console.log('Reading data');
//     setTimeout(() => {
//         callback({data: data});
//     }, 2000);
// }
//
// getData(5, function (data) {
//     console.log(data);
// });
// console.log('finish');

//////////////////
// ES6
//////////////////

const promise = new Promise((resolve, reject) => {
    //Here is asyc
    setTimeout(() => {
        reject(new Error('something went wrong'));
    }, 2000)
});

promise.then(data => {
    console.log(data);
}).catch(error => console.log(error));


// class ShoppingList {
//     constructor(items, number) {
//         this.items = items;
//         this.number = number;
//     }
//
//     sayList() {
//         console.log(this.items);
//     }
// }
//
// const myList = new ShoppingList(['Milk', 'Choco', 'Redbull'], 3);
//
// class Product extends ShoppingList {
//     constructor(items, number, amount, cost) {
//         super(items, number);
//         this.amount = amount;
//         this.cost = cost;
//     }
// }
//
// const product = new Product(['Milk', 'Choco', 'Redbull'], 3, 1, 2);
//
// product.sayList();

// const shoppingList = ['Milk', 'Cow', 'Eggs', 'Bananas', 'Choco'];
//
// const filterList = shoppingList.filter(item => {
//     return item !== 'Eggs';
// });
//
// console.log(filterList);


// const newList = shoppingList.map((item) => item + "new");
//
// console.log(newList);

//
//
// shoppingList.forEach((product, index) => {
//     console.log(`${product} nice, the index is ${index}`);
// });

//
// const add = (c = 1, d = 1) => {
//     console.log(c + d);
// }
//
// add();

// const user = {
//     name: 'David',
//     age: 23,
//     sayName: function () {
//         console.log(`My name is ${this.name}`);
//         const fullName = () => {
//             console.log(`My name is ${this.name} and my age is ${this.age}`);
//         };
//         fullName();
//     }
// };
//
// user.sayName();


// const sayLocation = (location) => {
//     console.log(`my location is unknown ${location}`);
// };
//
// const sayLocation = location => console.log(`my location is unknown ${location}`);
//
// sayLocation('paris');

// const list = {
//     name: 'Shopping List',
//     items: ['Milk', 'Cow']
// };
//
// const {name, items} = list;
//
// console.log(list)

// function getBook(title, author) {
//     return {
//         title,
//         author,
//         nonFunctionVariable: true,
//     }
// }
//
// const book = getBook('Harry Potter', 'JK');
//
// console.log(book);
// const name = 'Ed';
// const age = 25;
//
// console.log(`Hello it's my name is ${name}. My age is ${age}`);

// const todoList = ['Milk', 'Cow'];
// let count = 10;
// count = 10;
// console.log(count);
// const list = [1, 2, 3, 4, 5];
// for (var i = 0; i < list.length; i++) {
//     console.log(i);
// }